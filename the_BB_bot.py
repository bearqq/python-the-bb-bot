# coding: utf-8
# python3
#some definitions:

#imports:
from config import token

#telegram
import telebot

#chatterbot
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

#some global inits:
bot = telebot.TeleBot(token)

chatbot = ChatBot("testbot")
chatbot.set_trainer(ChatterBotCorpusTrainer)
chatbot.train("chatterbot.corpus.chinese")

@bot.message_handler(commands=['start'])
def send_welcome(message):
	bot.reply_to(message, "你好！")

# 'text', 'audio', 'document', 'photo', 'sticker', 'video', 'voice', 'location', 'contact', 'new_chat_participant', 'left_chat_participant', 'new_chat_title', 'new_chat_photo', 'delete_chat_photo', 'group_chat_created'
@bot.message_handler(content_types=['sticker'])
def handle_sticker(message):
	bot.reply_to(message, message.sticker)
	
@bot.message_handler(content_types=['text'])
def handle_text(message):
	#bot.reply_to(message, "然后呢？")
	bot.reply_to(message,chatbot.get_response(message.text))

@bot.message_handler(func=lambda message: True)
def echo_all(message):
    bot.reply_to(message, "请说人话")


if __name__ == '__main__':
	bot.polling()














"""
#commands available
#https://github.com/eternnoir/pyTelegramBotAPI

@bot.message_handler(func=lambda m: True)
def echo_all(message):
    bot.reply_to(message, message.text)

# Handles all text messages that match the regular expression
@bot.message_handler(regexp="SOME_REGEXP")
def handle_message(message):
    pass
    
"""

