# coding: utf-8
import codecs,os,sys,re

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

def process_the_file(file_name):
	F=codecs.open(file_name,'r','utf-8')
	lines=F.readlines()
	len_lines=len(lines)
	F.close()
	#print lines[5].encode('gb18030')
	if not lines[5].startswith(u"消息对象:"):
		print "Format error: %s" % file_name
		sys.exit()
	
	def process_talks(line_num):
		#line_num, isGoing, list2train
		talk=[]
		try:
			talk_day=re.search('(\d*-\d*-\d*\s).*',lines[line_num]).group(1)
		except:
			print line_num
			talk_day=''
		if not talk_day:
			return 0, 0, []

		def process_one_line(line_num):
			#one_line,last_line,end_of_file
			try:
				this_day=re.search('(^\d*-\d*-\d*\s).*',lines[line_num]).group(1)
			except:
				this_day=''

			if not this_day:
				return '',line_num,2
			
			if this_day!=talk_day:
				return '',line_num,1
			
			one_line=[]
			isInTalk=1
			i=1
			while isInTalk:
				l=lines[line_num+i].strip()
				if l:
					l=l.replace(u'[图片]',u'😂')
					l=l.replace(u'[表情]',u'😂')
					l=l.replace(u'"',"'")
					l=l.replace('\\','\\\\')
					l=l.replace(u'U','')
					one_line.append(l)
					i+=1
				else:
					if line_num+i+1>=len_lines:
						one_line=' '.join(one_line)
						return one_line,line_num+i+1,2
					elif re.search('(^\d*-\d*-\d*\s).*',lines[line_num+i+1]):
						isInTalk=0
					else:
						one_line.append(l)
						i+=1

			one_line=' '.join(one_line)
			return one_line,line_num+i+1,0
		
		#isSameDay=1
		while 1:
			one_line, line_num, end_of_file = process_one_line(line_num)
			if end_of_file==0:
				if one_line:
					talk.append(one_line)
			elif end_of_file==1:
				return line_num, 1, talk
			elif end_of_file==2:
				if one_line:
					talk.append(one_line)
				return 0, 0, talk

	line_num=8
	isGoing=1
	lists=[]
	while isGoing:
		line_num, isGoing, list2train = process_talks(line_num)
		if list2train:
			lists.append(list2train)
	O=codecs.open(file_name+'.json','w','utf-8')
	#O.write(str(lists))
	O.write("""{"conversations":[""")
	while 1:
		li=lists.pop()
	#for li in lists:
		O.write('["')
		O.write('","'.join(li))
		O.write('"]')
		if lists:
			O.write(',\r\n')
		else:
			break
	O.write("""]}""")
	O.close()
	print 'Done with %s' % file_name





if __name__ == "__main__":
	all_files=os.listdir('.')
	for fil in all_files:
		if fil.endswith('.txt'):
			process_the_file(fil)

